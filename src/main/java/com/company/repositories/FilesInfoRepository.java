package com.company.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.company.domain.entity.FileInfo;

import java.util.List;

/**

 */
public interface FilesInfoRepository extends JpaRepository<FileInfo, Long> {
    FileInfo findByStorageFileName(String fileName);

    List<FileInfo> findAll();

    List<FileInfo> findAllByFilm_Id(Long filmId);

    FileInfo findFileInfoById(Long fileId);

    FileInfo removeFileInfoById(Long fileId);


//    FileInfo getFileInfoById(Long fileId);
}
