package com.company.controllers;

import com.company.domain.dto.actorDto.AddActorForm;
import com.company.domain.dto.actorDto.UpdateActorDto;
import com.company.services.ActorsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


/**
 * by Aydar Zakirov
 * since 14.02.2022
 * This controller deals with actors.
 * It provides such operations as getting all actors, adding (if you are an admin), updating, deleting an actor
 */
@Controller
@RequiredArgsConstructor
public class ActorsController {

    private final ActorsService actorsService;

    @RequestMapping("/actors")
    public String getActorsPage(@AuthenticationPrincipal(expression = "account.isAdmin()") Boolean isAdmin,
                                Model model) {
        model.addAttribute("actors", actorsService.getAllActors());
        model.addAttribute("isAdmin", isAdmin);
        return "actors";
    }

    @RequestMapping(value = "/actors", method = RequestMethod.POST)
    public String addActor(AddActorForm form) {
        actorsService.addActor(form);
        return "redirect:/actors";
    }

    @PostMapping("/actors/delete")
    public String deleteActor(@RequestParam("actorId") Long actorId) {
        actorsService.deleteActorById(actorId);
        return "redirect:/actors";
    }

    @RequestMapping("/actors/delete")
    public String getActorsDeletePage(Model model) {
        model.addAttribute("actors", actorsService.getAllActors());
        return "actors";
    }

    @GetMapping("/actors/{actor-id}/update")
    public String getUpdateActor(@RequestParam("actorId") Long actorId, Model model) {
        model.addAttribute("actor", actorsService.getActor(actorId));
        return "update_actor_page";
    }

    @PostMapping(value = "/actors/{actor-id}/update")
    public String updateActor(@PathVariable("actor-id") Long actorId,
                              UpdateActorDto updatedActor) {
        actorsService.updateActor(actorId, updatedActor);
        return "redirect:/actors";
    }
}
