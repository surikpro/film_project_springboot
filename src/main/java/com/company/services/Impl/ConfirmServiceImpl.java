package com.company.services.Impl;

import com.company.domain.entity.Account;
import com.company.repositories.AccountsRepository;
import com.company.services.ConfirmService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**

 */
@Service
@RequiredArgsConstructor
public class ConfirmServiceImpl implements ConfirmService {

    private final AccountsRepository accountsRepository;

    @Override
    public boolean confirm(String uuid) {
        Optional<Account> account = accountsRepository.findByConfirmUUID(uuid);
        if (account.isPresent()) {
            Account forSave = account.get();
            forSave.setState(Account.State.CONFIRMED);
            accountsRepository.save(forSave);
            return true;
        }
        return false;
    }
}
