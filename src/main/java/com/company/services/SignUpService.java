package com.company.services;

import com.company.domain.dto.signUp.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);
}
