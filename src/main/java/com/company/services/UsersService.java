package com.company.services;

import com.company.domain.dto.actorDto.AccountDto;

import java.util.List;

public interface UsersService {
    List<AccountDto> getAllUsers();
    void deleteUser(Long userId);
}
