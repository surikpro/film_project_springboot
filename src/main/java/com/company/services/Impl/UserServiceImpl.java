package com.company.services.Impl;

import com.company.domain.dto.actorDto.AccountDto;
import com.company.domain.entity.Account;
import com.company.repositories.AccountsRepository;
import com.company.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UsersService {

    private final AccountsRepository accountsRepository;
    @Override
    public List<AccountDto> getAllUsers() {
        return AccountDto.from(accountsRepository.findAll());
    }

    @Override
    public void deleteUser(Long userId) {
        Account account = accountsRepository.getById(userId);
        account.setState(Account.State.DELETED);
        accountsRepository.save(account);
    }
}
